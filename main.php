<?php

	// Das hier kann die Übersichtsseite werden (Dashboard etc.).

	// Erstmal: SESSION starten, um Zugriff auf $_SESSION zu bekommen.
   session_start();
   
?>

<html>
<body>

<?php
	// Hier wird nur der in $_SESSION gespeicherte Benutzername angezeigt.
	echo 'Hallo '.$_SESSION['login'].'!';
?>

<br><br>
<!-- Hier ist ein statischer Link zur Logout-Seite. -->

<a href="logout.php">Logout</a>

</body>
</html>