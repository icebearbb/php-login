<?php

// Zugangsdaten zum Server
$dbname='logindb';
$dbhost='localhost';
$dbuser='root';
$dbpass='';

// Erstellen der Datenbankverbindung, gespeichert in der Variablen $link
$link = mysqli_connect($dbhost,$dbuser,$dbpass);

// Einstellen der Übertragungskodierung als utf8, wichtig für Umlaute.
mysqli_set_charset($link,'utf8');

// Auswahl der Datenbank.
mysqli_select_db($link,$dbname);

?>
