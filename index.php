<?php

   // Bei jeder dynamischen Seite muss die SESSION gestartet werden.
   // Sie wird auf dem Server gespeichert und enth�lt Variablen, die 
   // einem Browser zugeordnet sind. Das hei�t: PHP wei�, welcher
   // Browser und damit welcher User zu welcher SESSION geh�rt.
   
   // Beispiel: Wenn ich mich mit dem Server verbinde und als 'ich'
   // anmelde, hat die Variable $_SESSION['login'] den Wert 'ich'.
   // Wenn Peter sich als 'pete42' anmeldet, hat f�r ihn die Variable
   // den Wert 'pete42'.
   
   // Die Variablen k�nnen beliebig benannt werden,
   // z.B. $_SESSION['user'], $_SESSION['score'], $_SESSION['value'] ...
   session_start();

   // Im Array $_POST befinden sich alle Werte, die im Formular eingegeben
   // wurden, zu finden unter den Indizes ['uid'] und ['pwd'].
   if ($_POST) {
	   
	  // Die Benutzerdaten sollen aus der Datenbank abgerufen werden, also
	  // Datenbank starten.
	  include 'dbconnect.inc.php';
	  
	  // Der Benutzername wird in einer SQL-Abfrage verwendet, also
	  // vor SQL-Injections sichern.
	  $uid=mysqli_real_escape_string ($link,$_POST['uid']);
	  
	  // F�r das Passwort wird ein SHA256-Hash gebildet.
	  $pwd=$_POST['pwd'];
	  $pwd=hash('sha256', $_POST['pwd']);

	  // Wenn Benutzername und Passwort nicht leer sind, ...
	  if (($uid!='') && ($pwd!=hash('sha256', ''))) {
		
		// abfragen, ob die eingegebenen Daten mit denen in der Datenbank �bereinstimmen.
		$res=mysqli_query ($link,"SELECT login FROM user WHERE login='$uid' AND pass='$pwd'");
		
		// Wenn es einen Eintrag gibt, ...
		if ($res) {
			// nochmal pr�fen, ob es auch der richtige ist.
			$log=mysqli_fetch_row($res);  // erste Zeile des Ergebnisses abrufen
			// Wenn der Benutzername wirklich stimmt, dann...
			if ($log[0]==$uid) {
				// Benutzernamen in $_SESSION eintragen
				$_SESSION['login']=$uid;
				// Zur Hauptseite (�bersicht, Dashboard, etc.) wechseln.
				header ('Location: main.php');
				// Dieses Skript beenden.
				exit();
			} else {
				// sonst merken, dass das Login fehlgeschlagen ist.
				$_SESSION['logfail']='-';
			}
		} else {
			// sonst merken, dass das Login fehlgeschlagen ist.
			$_SESSION['logfail']='-';
		}

	  }

   }

?>
<html>
<body>
<br><br>

<?php 
    // Falls $_SESSION['logfail'] gesetzt ist, anzeigen, dass das Login
	// fehlgeschlagen ist.
	if (isset($_SESSION['logfail'])) echo "Benutzername oder Passwort falsch!";
	
	// Hier werden die Session-Informationen gel�scht, damit der Benutzer
	// sich neu anmelden kann. Wenn das Login klappt, dann beendet sich das Skript 
	// schon in Zeile 50 und dieser Befehl hier wird nicht mehr ausgef�hrt.
	session_destroy();
 ?>
 
<br>
	<!-- Hier ist das Anmeldeformular in HTML. Die Namen der Eingabefelder
	werden oben im PHP-Skript im Array $_POST verwendet. -->
   Bitte melden Sie sich an:
   <form action="index.php" method="POST">
   <table><tr><td>
   Benutzer:</td><td><input type="text" name="uid"></td>
   </tr><tr><td>
   Passwort:</td><td><input type="password" name="pwd"></td>
   </tr></table>
   <input type="submit" value="Anmelden">
   </form>
   
</body>
</html>