# php-login

A simple dynamic user page with login in PHP. Uses MySQL.

## required software

* XAMPP 

## install

* Clone repo into xampp/htdocs/php-login.
* Start Apache and MySQL server.
* Open address 'localhost/phpmyadmin' in a browser.
* Create a database named **logindb**.
* Import 'logindb.sql' into database.
* Only if you're not using XAMPP locally: Set database user & password in 'dbconfig.inc.php'.
* Open address 'localhost/php-login' in a browser.
* Enter 'ich' as user name and 'lol' as password.
* Click 'Einloggen'. 
* You should see 'Hallo ich!'.

## documentation

See comments in the code (German).